package com.liangzili.demos.slice;

import com.liangzili.demos.ResourceTable;
import com.liangzili.demos.database.PreferenceDataBase;
import com.liangzili.demos.utils.CookieUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.webengine.*;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;
import ohos.net.http.SslError;
import ohos.utils.net.Uri;

import java.util.HashMap;
import java.util.Map;

public class MainAbilitySlice extends AbilitySlice {
    private static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0x0, AbilitySlice.class.getName());

    //DatabaseHelper的构造需要传入context，Ability和AbilitySlice都实现了ohos.app.Context接口。因此可以从应用中的Ability或AbilitySlice调用getContext()方法来获得context。
    private Context context;
    //偏好数据库
    private Preferences preferences;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        HiLog.info(TAG, "onStart时调用");

        String url = "https://m.bilibili.com";
//        String url = "https://passport.bilibili.com/login";

        //获取偏好数据库实例
        context = getContext();
        preferences = PreferenceDataBase.register(context,"bilibili");
        //从偏好数据库中读取cookie
        CookieUtils.ExtarctCookie(preferences,url);

        //启动webview
        webview(url);
    }
    //启动webview
    public void webview(String url){
        WebView webView = (WebView) findComponentById(ResourceTable.Id_webview);
        webView.getWebConfig().setJavaScriptPermit(true);  // 如果网页需要使用JavaScript，增加此行；如何使用JavaScript下文有详细介绍
        webView.load(url);

        webView.setWebAgent(new WebAgent() {
            public static final String EXAMPLE_URL = "...";
            @Override  //当Web页面进行链接跳转时，WebView默认会打开目标网址，通过以下方式可以定制该行为。
            public boolean isNeedLoadUrl(WebView webview, ResourceRequest request) {
                if (request == null || request.getRequestUrl() == null) {
                    return false;
                }
                Uri uri = request.getRequestUrl();
                System.out.println("这里应该是链接跳转时" + uri);
                // EXAMPLE_URL由开发者自定义
                if (uri.getDecodedHost().equals(EXAMPLE_URL)) {
                    // 增加开发者自定义逻辑
                    return false;
                } else {
                    return super.isNeedLoadUrl(webview, request);
                }
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onBackground() {
        HiLog.info(TAG, "Page不再对用户可见时调用");
        CookieUtils.SaveCookie(preferences,"https://m.bilibili.com");
    }

    @Override
    protected void onStop() {
        HiLog.info(TAG, "系统将要销毁Page时调用");
    }
}
